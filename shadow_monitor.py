import time
import smtplib
from email.mime.text import MIMEText
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import pythonsecrets

def send_email(subject, body):
    # Configura los detalles del correo electrónico
    sender = pythonsecrets.sender
    recipient = pythonsecrets.recipient
    password = pythonsecrets.password

    # Crea el objeto MIMEText con el cuerpo del mensaje
    message = MIMEText(body)
    message['Subject'] = subject
    message['From'] = sender
    message['To'] = recipient

    # Conecta y autentica con el servidor SMTP
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(sender, password)

    # Envía el correo electrónico
    server.send_message(message)
    server.quit()

class DirectoryChangeHandler(FileSystemEventHandler):
    def on_any_event(self, event):
        if event.is_directory and event.src_path == '/etc':
            subject = 'Cambio detectado en el directorio /etc'
            body = 'Se ha detectado un cambio dentro del directorio /etc.'
            send_email(subject, body)

if __name__ == "__main__":
    path = "/etc"  # Ruta del directorio a monitorear
    event_handler = DirectoryChangeHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(600)  # Espera 10 minutos (600 segundos)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
